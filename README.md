=== How to setup simple-weather-web-app project ===
---------------------------------------------
1. In your terminal, locate to the root of this project, npm install
2. Open src/config.js and follow the instructions.
3. npm start 
4. You are good to go!



=== About simple-weather-web-app ===
--------------------------------
simple-weather-web-app is a web app that able to see current weather where the city you desire.



=== How to use simple-weather-web-app ===
-------------------------------------
1. Enter a city name
2. Select refresh interval, initially is 1 minute.
3. Click [ Add to List ] , you can see that the city is added in the weather list.
4. You will able to see the weather details like temperature, weather conditions and more.
5. If the city is invalid, it is not able adding into weather list.