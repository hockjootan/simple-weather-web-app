export const intervalList = [
  {
    label: "1 Minute",
    value: 60000,
  },
  {
    label: "2 Minutes",
    value: 120000,
  },
  {
    label: "3 Minutes",
    value: 180000,
  },
];
