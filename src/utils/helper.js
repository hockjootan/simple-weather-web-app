export const _findOption = (list, id) => {
  return list.find((option) => String(option.value) === String(id));
};
