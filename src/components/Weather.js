import React, { useEffect } from "react";
import _ from "lodash";
import styled from "styled-components";

const Weather = (props) => {
  const { index, weather } = props;
  const { interval, data } = weather;

  useEffect(() => {
    const requestInterval = setInterval(() => {
      props._getWeather(index);
    }, interval);
    return () => clearInterval(requestInterval);
  }, []);

  return !_.isEmpty(data) ? (
    <WeatherWrapper key={index}>
      <WeatherCity>{data.name}</WeatherCity>
      <WeatherIcon
        src={`http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`}
      />
      <Remove className="remove" onClick={() => props._removeTiles(index)}>
        x
      </Remove>
      <WeatherDetails>
        <Temperature>{data.main.temp} &#8451;</Temperature>
        <WeatherCondition>{data.weather[0].main}</WeatherCondition>
      </WeatherDetails>
    </WeatherWrapper>
  ) : null;
};

const WeatherWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #c0c0c0;
  padding: 0.5rem;
  border-radius: 15px;
  max-height: 200px;
  position: relative;

  &:hover {
    .remove {
      opacity: 1;
      transition: opacity 0.2s;
    }
  }
`;

const WeatherCity = styled.div`
  font-size: 18px;
`;

const WeatherIcon = styled.img`
  width: 50%;
  align-self: center;
`;

const Remove = styled.div`
  cursor: pointer;

  width: 30px;
  height: 30px;

  opacity: 0;
  background-color: #fa8072;
  color: white;
  border-radius: 0px 15px 0px 15px;

  position: absolute;
  right: 0;
  top: 0;

  display: flex;
  justify-content: center;
  align-items: center;
`;

const WeatherDetails = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Temperature = styled.div`
  font-size: 18px;
`;

const WeatherCondition = styled.div`
  font-size: 18px;
`;

export default Weather;
