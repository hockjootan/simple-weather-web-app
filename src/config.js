/* 
    Open Weather API , Complete API docs : https://openweathermap.org/current
    1. Register an account and get your free API key at https://home.openweathermap.org/users/sign_up 
    2. Put your API key in OPEN_WEATHER_API_KEY , noted that the API key takes a while to make it available. You can get more details from their email.
    3. You can change weather units value too, refer the docs : https://openweathermap.org/current#data
 */

export const OPEN_WEATHER_API_KEY = "YOUR_KEY";
export const WEATHER_UNITS = "metric";
