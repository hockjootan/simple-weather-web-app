import React, { useState } from "react";
import Select from "react-select";
import axios from "axios";
import produce from "immer";
import styled from "styled-components";

// UI Components
import Weather from "./components/Weather";

// Helper
import { intervalList } from "./utils/constant";
import { _findOption } from "./utils/helper";

// Config
import { OPEN_WEATHER_API_KEY, WEATHER_UNITS } from "./config";

const App = () => {
  const [weatherList, setWeatherList] = useState([]);
  const [city, setCity] = useState("");
  const [interval, setInterval] = useState(60000);

  const _addTiles = () => {
    if (city === "" || city.length === 0) {
      alert("City should not be empty");
      return;
    }

    axios
      .get(
        `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=${WEATHER_UNITS}&appid=${OPEN_WEATHER_API_KEY}`
      )
      .then((result) => {
        const newWeather = { city, interval, data: result.data };
        setWeatherList((weatherList) => {
          return produce(weatherList, (draft) => {
            draft.push(newWeather);
          });
        });
        setCity("");
      })
      .catch((err) => {
        alert("City not found!");
      });
  };

  const _getWeather = (index) => {
    axios
      .get(
        `http://api.openweathermap.org/data/2.5/weather?q=${weatherList[index].city}&units=${WEATHER_UNITS}&appid=${OPEN_WEATHER_API_KEY}`
      )
      .then((result) => {
        setWeatherList((weatherList) => {
          return produce(weatherList, (draft) => {
            draft[index].data = result.data;
          });
        });
      });
  };

  const _removeTiles = (index) => {
    setWeatherList((weatherList) => {
      return weatherList.filter((weather, weatherIndex) => {
        return weatherIndex !== index;
      });
    });
  };

  return (
    <MainWrapper>
      <WeatherListWrapper>
        {weatherList.map((weather, index) => (
          <Weather
            key={index}
            index={index}
            weather={weather}
            _getWeather={_getWeather}
            _removeTiles={_removeTiles}
          />
        ))}
      </WeatherListWrapper>
      <InputGroup>
        <TextLabel>Enter City</TextLabel>
        <Input
          type="text"
          onChange={(e) => setCity(e.target.value)}
          value={city}
        />
        <TextLabel>Refresh Interval</TextLabel>
        <Select
          options={intervalList}
          onChange={(option) => setInterval(option.value)}
          value={_findOption(intervalList, interval)}
        />
        <Confirm onClick={() => _addTiles()}>Add to List</Confirm>
      </InputGroup>
    </MainWrapper>
  );
};

const MainWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 2rem;
`;

const WeatherListWrapper = styled.div`
  width: 80%;
  max-height: 50%;
  overflow-y: scroll;

  display: grid;
  grid-grap: 2rem;
  gap: 2rem;
  grid-template-columns: repeat(3, minmax(0, 1fr)) !important;
`;

const InputGroup = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 2rem;
`;

const TextLabel = styled.div`
  font-size: 18px;
  margin-bottom: 0.5rem;
`;

const Input = styled.input`
  padding: 0.5rem;
  margin-bottom: 0.5rem;
`;

const Confirm = styled.div`
  margin-top: 0.5rem;
  padding: 1rem;
  border-radius: 5px;
  background-color: black;
  color: white;
  display: flex;
  justify-content: center;

  &:hover {
    cursor: pointer;
    background-color: #708090;
    transition: background-color 0.3s;
  }
`;

export default App;
